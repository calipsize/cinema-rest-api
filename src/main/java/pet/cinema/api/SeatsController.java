package pet.cinema.api;


import io.micrometer.common.util.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pet.cinema.exceptions.UnauthorizedException;
import pet.cinema.model.ReturnedTicket;
import pet.cinema.model.Seat;
import pet.cinema.model.Ticket;
import pet.cinema.model.Statistics;
import pet.cinema.service.SeatsService;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api")
public class SeatsController {
    private final SeatsService seatsServices;

    @Autowired
    public SeatsController(SeatsService seatsServices) {
        this.seatsServices = seatsServices;
    }


    @PostMapping("/purchase")
    public Ticket getTicket(@RequestBody Seat seat) {
        return seatsServices.getTicketService(seat);
    }

    @PostMapping("/return/{token}")
    public ReturnedTicket returnTicket(@PathVariable("token") UUID token) {
        return seatsServices.returnTicketService(token);
    }

    @GetMapping("/seats")
    public List<Seat> showSeats() {
        return seatsServices.getAvailableSeatsService();
    }

    @PostMapping("/stats")
    public Statistics showStatistics(@RequestParam(required = false) String password) {
        if (StringUtils.isBlank(password)) throw new UnauthorizedException("The password is wrong!");

        if (password.equals("super_secret"))
            return seatsServices.getStatisticsService();
        else throw new UnauthorizedException("The password is wrong!");
    }
}
