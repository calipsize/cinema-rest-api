package pet.cinema.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Ticket {
    @JsonProperty("ticket")
    private Seat ticket;
}
