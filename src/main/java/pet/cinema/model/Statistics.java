package pet.cinema.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity(name = "statistics")
public class Statistics {
    @Id
    @GeneratedValue
    private Long id;

    @JsonProperty("current_income")
    private Double currentIncome;

    @JsonProperty("available_seats")
    private Integer availableSeats;

    @JsonProperty("purchased_tickets")
    private Integer purchasedTickets;

    public Statistics(Double currentIncome, Integer availableSeats, Integer purchasedTickets) {
        this.currentIncome = currentIncome;
        this.availableSeats = availableSeats;
        this.purchasedTickets = purchasedTickets;
    }
}
