package pet.cinema.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.util.UUID;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity(name = "seats")
public class Seat {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("seatRow")
    Integer seatRow;

    @JsonProperty("seatColumn")
    Integer seatColumn;

    @JsonProperty("price")
    Double price;

    @JsonProperty("availability")
    boolean status;

    public Seat(Integer seatRow, Integer seatColumn, Double price, boolean status) {
        this.seatRow = seatRow;
        this.seatColumn = seatColumn;
        this.price = price;
        this.status = status;
    }

    public Seat(Integer seatRow, Integer seatColumn) {
        this.seatRow = seatRow;
        this.seatColumn = seatColumn;
    }
}
