package pet.cinema.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ReturnedTicket {
    @JsonProperty("returned_ticket")
    private Seat ticket;
}
