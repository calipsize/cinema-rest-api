package pet.cinema.dao.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pet.cinema.model.Seat;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface SeatsRepository extends JpaRepository<Seat, Long> {
    Optional<Seat> findBySeatRowAndSeatColumn(Integer row, Integer column);

    boolean existsBySeatRowAndSeatColumnAndStatus(Integer row, Integer column, boolean status);

    boolean existsBySeatRowAndSeatColumn(Integer row, Integer column);

    Optional<Seat> findById(UUID token);

    boolean existsById(UUID token);
}
