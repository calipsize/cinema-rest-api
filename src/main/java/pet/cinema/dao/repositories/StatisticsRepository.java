package pet.cinema.dao.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pet.cinema.model.Statistics;


@Repository
public interface StatisticsRepository extends JpaRepository<Statistics, Long> {

}
