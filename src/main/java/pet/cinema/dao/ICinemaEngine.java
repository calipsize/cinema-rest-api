package pet.cinema.dao;


import pet.cinema.model.ReturnedTicket;
import pet.cinema.model.Seat;
import pet.cinema.model.Ticket;
import pet.cinema.model.Statistics;

import java.util.List;
import java.util.UUID;


public interface ICinemaEngine {
    Ticket getTicket(Seat seat);

    ReturnedTicket returnTicket(UUID token);

    List<Seat> getAvailableSeats();

    Statistics getStatistics();
}
