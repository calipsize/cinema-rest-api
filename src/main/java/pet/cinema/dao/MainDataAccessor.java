package pet.cinema.dao;


import io.micrometer.common.util.StringUtils;

import org.springframework.stereotype.Repository;

import pet.cinema.dao.repositories.SeatsRepository;
import pet.cinema.dao.repositories.StatisticsRepository;

import pet.cinema.exceptions.BadRequestException;
import pet.cinema.exceptions.NotFoundException;

import pet.cinema.model.ReturnedTicket;
import pet.cinema.model.Seat;
import pet.cinema.model.Ticket;
import pet.cinema.model.Statistics;

import java.util.*;


@Repository("DataAccessor")
public class MainDataAccessor implements ICinemaEngine {
    public static final int MAX_SEATS_IN_ROW = 9;

    private final StatisticsRepository statisticsRepository;
    private final SeatsRepository seatsRepository;

    public MainDataAccessor(StatisticsRepository statisticsRepository, SeatsRepository seatsRepository) {

        this.statisticsRepository = statisticsRepository;
        statisticsRepository.save(new Statistics(0d, 81, 0));


        this.seatsRepository = seatsRepository;
        List<Seat> tempAvailableSeatsList = new ArrayList<>();

        for (int i = 1; i <= MAX_SEATS_IN_ROW; i++) {
            for (int j = 1; j <= MAX_SEATS_IN_ROW; j++) {
                if (i <= 4) {
                    tempAvailableSeatsList.add(new Seat(i, j, 10d, false));
                } else {
                    tempAvailableSeatsList.add(new Seat(i, j, 8d, false));
                }
            }
        }

        seatsRepository.saveAll(tempAvailableSeatsList);
    }

    public void soldTicket(Double price) {
        Statistics tempStatistics = statisticsRepository
                .findById(1L)
                .orElseThrow(() -> new NotFoundException("Not found!"));

        tempStatistics.setCurrentIncome(+price);

        int temp = tempStatistics.getAvailableSeats();

        tempStatistics.setAvailableSeats(--temp);

        temp = tempStatistics.getPurchasedTickets();

        tempStatistics.setPurchasedTickets(++temp);

        statisticsRepository.save(tempStatistics);
    }

    public void returnSoldTicket(Double price) {
        Statistics tempStatistics = statisticsRepository
                .findById(1L)
                .orElseThrow(() -> new NotFoundException("Not found!"));

        tempStatistics.setCurrentIncome(tempStatistics.getCurrentIncome()-price);

        int temp = tempStatistics.getAvailableSeats();

        tempStatistics.setAvailableSeats(++temp);

        temp = tempStatistics.getPurchasedTickets();

        tempStatistics.setPurchasedTickets(--temp);

        statisticsRepository.save(tempStatistics);
    }

    public Ticket getTicket(Seat selectedSeat) {
        if (StringUtils.isBlank(selectedSeat.getSeatRow().toString())
        || StringUtils.isBlank(selectedSeat.getSeatColumn().toString()))
            throw new BadRequestException("Try to enter the full data!");

        if (seatsRepository.existsBySeatRowAndSeatColumnAndStatus(selectedSeat.getSeatRow(), selectedSeat.getSeatColumn(), true))
            throw new BadRequestException("The ticket has been already purchased!");

        Seat tempSeat = seatsRepository
                .findBySeatRowAndSeatColumn(selectedSeat.getSeatRow(), selectedSeat.getSeatColumn())
                .orElseThrow(() -> new NotFoundException("The selected seat is not found!"));


        tempSeat.setStatus(true);

        seatsRepository.flush();


        soldTicket(tempSeat.getPrice());

        return new Ticket(tempSeat);
    }


    public ReturnedTicket returnTicket(UUID token) {
        if (StringUtils.isBlank(token.toString()))
            throw new BadRequestException("Try to enter the full data!");

        Seat tempReturnedTicket = seatsRepository
                .findById(token)
                .orElseThrow(() -> new NotFoundException("The seat is not found or token is wrong. Try again!"));

        tempReturnedTicket.setStatus(false);
        seatsRepository.flush();

        returnSoldTicket(tempReturnedTicket.getPrice());

        return new ReturnedTicket(tempReturnedTicket);
    }

    @Override
    public List<Seat> getAvailableSeats() {
        List<Seat> tempAllAvailableSeats = seatsRepository.findAll();

        if (tempAllAvailableSeats.isEmpty()) throw new NotFoundException("The list is empty");

        return tempAllAvailableSeats;
    }

    @Override
    public Statistics getStatistics() {
        return statisticsRepository
                .findById(1L)
                .orElseThrow(() -> new NotFoundException("Not found!"));
    }
}
