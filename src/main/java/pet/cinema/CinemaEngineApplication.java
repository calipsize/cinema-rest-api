package pet.cinema;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CinemaEngineApplication {
    public static void main(String[] args) {
        SpringApplication.run(CinemaEngineApplication.class, args);
    }
}
