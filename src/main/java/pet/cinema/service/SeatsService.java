package pet.cinema.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import pet.cinema.dao.ICinemaEngine;

import pet.cinema.model.ReturnedTicket;
import pet.cinema.model.Seat;
import pet.cinema.model.Ticket;
import pet.cinema.model.Statistics;

import java.util.List;
import java.util.UUID;


@Service
public class SeatsService {
    private final ICinemaEngine iCinemaEngine;

    @Autowired
    public SeatsService(@Qualifier("DataAccessor") ICinemaEngine iCinemaEngine) {
        this.iCinemaEngine = iCinemaEngine;
    }

    public Ticket getTicketService(Seat seat) {
        return iCinemaEngine.getTicket(seat);
    }

    public ReturnedTicket returnTicketService(UUID token) {
        return iCinemaEngine.returnTicket(token);
    }

    public List<Seat> getAvailableSeatsService() {
        return iCinemaEngine.getAvailableSeats();
    }

    public Statistics getStatisticsService() {
        return iCinemaEngine.getStatistics();
    }
}
